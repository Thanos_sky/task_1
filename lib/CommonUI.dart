import 'package:flutter/material.dart';


  Widget createCenterButton(String name,Function onPress) {
    var button = Center(
      child: SizedBox(
        width: 200,
        child: FlatButton(
          onPressed: (){
            onPress();
          },
          color: Colors.orange,
          child: Text(name,style: TextStyle(color: Colors.white),),
        ),
      ),
    );
    return button;
  }


