import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:coindcx/Page1.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'Routes.dart';

void main() => runApp(MaterialApp(
      home: Navigationtask(),
    ));
GlobalKey<ScaffoldState> navKey = GlobalKey<ScaffoldState>();

class Navigationtask extends StatefulWidget {
  @override
  _NavigationtaskState createState() => _NavigationtaskState();
}

class _NavigationtaskState extends State<Navigationtask> {
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  StreamController<ConnectivityResult> _streamController =
      StreamController.broadcast();

  Stream get _streamConnection => _streamController.stream;
  var isNetworkerror = false;

  @override
  void initState() {
    super.initState();
    check();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen((value) {
      _streamController.add(value);
    });
    _streamController.stream.listen((data) {
      data == null ? isNetworkerror = false : isNetworkerror = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: navKey,
      body: WillPopScope(
        onWillPop: _OnWillPop,
        child: StreamBuilder<ConnectivityResult>(
            stream: _streamConnection,
            builder: (context, snapshot) {
              var data = snapshot.data;
              var txt = "Connected to Network ..!!";
              if (data != null) {
                if (data == ConnectivityResult.none) txt = "Network error ..!!";
              }
              return Stack(
                children: <Widget>[
                  MaterialApp(
                    initialRoute: Routes.page1,
                    onGenerateRoute: Routes.generatePage,
                    home: StreamBuilder<ConnectivityResult>(
                        stream: _streamConnection,
                        builder: (context, snapshot) {
                          return Scaffold(body: Page_1());
                        }),
                  ),
                  (data != null)
                      ? Center(
                          child: Card(
                            color: Colors.transparent,
                            elevation: 8.0,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              child: Container(
                                width: double.maxFinite,
                                height: 150,
                                color: Colors.orange,
                                child: Center(
                                    child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: 40,
                                    ),
                                    Expanded(child: Text(txt)),
                                    FlatButton(
                                      onPressed: () {
                                        _streamController.add(null);
                                      },
                                      child: Text("OK"),
                                      color: Colors.green,
                                    )
                                  ],
                                )),
                              ),
                            ),
                          ),
                        )
                      : SizedBox.shrink()
                ],
              );
            }),
      ),
    );
  }

  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  Future<bool> _OnWillPop() async {
    return !isNetworkerror;
  }
}
