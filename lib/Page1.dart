import 'package:coindcx/CommonUI.dart';
import 'package:flutter/material.dart';

import 'Routes.dart';

class Page_1 extends StatefulWidget {
  @override
  _Page_1State createState() => _Page_1State();
}

class _Page_1State extends State<Page_1> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      child: createCenterButton("Screen A", () {
        Navigator.pushNamed(context, Routes.page2);
      }),
    );
  }
}
