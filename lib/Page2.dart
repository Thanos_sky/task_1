import 'package:flutter/material.dart';

import 'CommonUI.dart';
import 'Routes.dart';

class Page_2 extends StatefulWidget {
  @override
  _Page_2State createState() => _Page_2State();
}

class _Page_2State extends State<Page_2> {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.red,
        child: createCenterButton("Screen B", () {
          Navigator.pushNamed(context, Routes.page3);
        }));
  }
}
