import 'package:flutter/material.dart';

import 'CommonUI.dart';
import 'Routes.dart';

class Page_3 extends StatefulWidget {
  @override
  _Page_3State createState() => _Page_3State();
}

class _Page_3State extends State<Page_3> {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.green,
        child: createCenterButton("Screen C", () {
          Navigator.pushNamed(context, Routes.page1);
        }));
  }
}
