import 'package:coindcx/Page1.dart';
import 'package:coindcx/Page2.dart';
import 'package:coindcx/Page3.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Routes {
  static const page1 = "/";
  static const page2 = "/page2";
  static const page3 = "/page3";

  static Route<dynamic> generatePage(RouteSettings settings) {
    switch (settings.name) {
      case page1:
        return MaterialPageRoute(builder: (context) => Page_1());
      case page2:
        return MaterialPageRoute(builder: (context) => Page_2());
      case page3:
        return MaterialPageRoute(builder: (context) => Page_3());
    }
  }
}
